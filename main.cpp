#include <iostream>
#include <utility>
#include <ctime>
#include <string>

using namespace std;

class WrongPositionException : public invalid_argument {
public:
    explicit WrongPositionException(string message) :
            invalid_argument(message), msg_(move(message)) {}

    ~WrongPositionException() noexcept override = default;

    [[nodiscard]] const char *what() const noexcept override {
        return msg_.c_str();
    }

protected:
    string msg_;
};

class CollisionException : public invalid_argument {
public:
    explicit CollisionException(string message) :
            invalid_argument(message), msg_(move(message)) {}

    ~CollisionException() noexcept override = default;

    [[nodiscard]] const char *what() const noexcept override {
        return msg_.c_str();
    }

protected:
    string msg_;
};

char chooseFirstPlayer();

char generateRandomPlayer();

void display(char board[3][3]);

void handleWrongPositionFormat();

void exitProgram(bool &exitFlag, int i);

void repeatGame(bool &repeatGameFlag, int i, int oWinStreak, int xWinStreak);

void markAnswerOnBoard(char (&board)[3][3], int positionY, int positionX, char character);

bool checkIfWin(char board[3][3], char character);

bool checkIfDraw(char board[3][3]);

void addWins(bool xWin, bool oWin, bool draw, bool repeatGameFlag, int &xWinsNumber, int &xWinStreak, int &oWinsNumber,
             int &oWinStreak);

int main() {
    int boardPositionX;
    int boardPositionY;
    bool exitFlag = false;
    int xWinsNumber = 0;
    int oWinsNumber = 0;
    int xWinStreak = 0;
    int oWinStreak = 0;
    bool repeatGameFlag;

    do {
        repeatGameFlag = false;

        char board[3][3] = {
                {'-', '-', '-'},
                {'-', '-', '-'},
                {'-', '-', '-'},
        };

        bool xWin = false;
        bool oWin = false;
        bool draw = false;

        cout << "*** KOLKO I KRZYZYK ***" << endl;
        cout << R"(Jesli chcesz rozpoczac od nowa - wcisnij "8", jesli chcesz zakonczyc gre - wcisnij "9".)" << endl;
        char nextRound = chooseFirstPlayer();

        system("CLS");

        while (!exitFlag && !repeatGameFlag && !xWin && !oWin && !draw) {
            display(board);

            switch (nextRound) {
                case 'O':
                    cout << "TURA KOLKA" << endl;
                    cout
                            << "Podaj pozycje na osi poziomej."
                            << endl;

                    cin >> boardPositionX;
                    if (cin.fail()) {
                        handleWrongPositionFormat();
                        break;
                    }

                    exitProgram(exitFlag, boardPositionX);
                    repeatGame(repeatGameFlag, boardPositionX, oWinStreak, xWinStreak);
                    if (exitFlag || repeatGameFlag) {
                        system("CLS");
                        break;
                    }

                    cout
                            << "Podaj pozycje na osi pionowej."
                            << endl;
                    cin >> boardPositionY;
                    if (cin.fail()) {
                        handleWrongPositionFormat();
                        break;
                    }

                    exitProgram(exitFlag, boardPositionY);
                    repeatGame(repeatGameFlag, boardPositionY, oWinStreak, xWinStreak);
                    if (exitFlag || repeatGameFlag) {
                        system("CLS");
                        break;
                    }

                    system("CLS");

                    try {
                        markAnswerOnBoard(board, boardPositionY, boardPositionX, 'O');
                        oWin = checkIfWin(board, 'O');

                        if (oWin) {
                            cout << "Gratulacje graczu \"O\"! Wygrales!" << endl;
                        } else if (checkIfDraw(board)) {
                            cout << "REMIS" << endl;
                            draw = true;
                        } else {
                            nextRound = 'X';
                        }
                    } catch (const WrongPositionException &e) {
                        cout << "Podales niepoprawna pozycje w tablicy" << endl;
                    } catch (const CollisionException &e) {
                        cout << "Nie mozna wpisywac znakow w wypelnionych polach" << endl;
                    }

                    break;
                case 'X':
                    cout << "TURA KRZYZYKA" << endl;
                    cout
                            << "Podaj pozycje na osi poziomej."
                            << endl;

                    cin >> boardPositionX;
                    if (cin.fail()) {
                        handleWrongPositionFormat();
                        break;
                    }

                    exitProgram(exitFlag, boardPositionX);
                    repeatGame(repeatGameFlag, boardPositionX, oWinStreak, xWinStreak);
                    if (exitFlag || repeatGameFlag) {
                        system("CLS");
                        break;
                    }

                    cout
                            << "Podaj pozycje na osi pionowej."
                            << endl;
                    cin >> boardPositionY;
                    if (cin.fail()) {
                        handleWrongPositionFormat();
                        break;
                    }

                    exitProgram(exitFlag, boardPositionY);
                    repeatGame(repeatGameFlag, boardPositionY, oWinStreak, xWinStreak);
                    if (exitFlag || repeatGameFlag) {
                        system("CLS");
                        break;
                    }

                    system("CLS");

                    try {
                        markAnswerOnBoard(board, boardPositionY, boardPositionX, 'X');
                        xWin = checkIfWin(board, 'X');

                        if (xWin) {
                            cout << "Gratulacje graczu \"X\"! Wygrales!" << endl;
                        } else if (checkIfDraw(board)) {
                            cout << "REMIS" << endl;
                            draw = true;
                        } else {
                            nextRound = 'O';
                        }
                    } catch (const WrongPositionException &e) {
                        cout << "Podales niepoprawna pozycje w tablicy" << endl;
                    } catch (const CollisionException &e) {
                        cout << "Nie mozna wpisywac znakow w wypelnionych polach" << endl;
                    }

                    break;
            }
        }

        display(board);
        cout << "Koniec gry. " << endl;

        addWins(xWin, oWin, draw, repeatGameFlag, xWinsNumber, xWinStreak, oWinsNumber, oWinStreak);

        if (xWinStreak >= 2) {
            cout << "Gracz \"X\" wygral " << xWinStreak << " z rzedu!" << endl;
        }

        if (oWinStreak >= 2) {
            cout << "Gracz \"O\" wygral " << oWinStreak << " z rzedu!" << endl;
        }

        cout << "Ilosc wygranych gracza \"X\": " << xWinsNumber << endl;
        cout << "Ilosc wygranych gracza \"O\": " << oWinsNumber << endl;

        if (!repeatGameFlag && !exitFlag) {
            cout << "Chcesz zagrac od nowa?" << endl;
            cout << "1. TAK" << endl;
            cout << "2. NIE" << endl;
            int option;
            cin >> option;

            while (cin.fail() || option < 1 || option > 2) {
                cin.clear();
                cin.ignore(numeric_limits<streamsize>::max(), '\n');
                cout << "Wybrano niepoprawna opcje, sprobuj jeszcze raz" << endl;
                cin >> option;
            }

            if (option == 1) {
                repeatGameFlag = true;
            }
        }

        if (repeatGameFlag) {
            system("CLS");
            cout << "Rozpoczyna sie kolejna rozgrywka." << endl;
        }

    } while (repeatGameFlag);

    return 0;
}

char chooseFirstPlayer() {
    cout << "Wybierz gracza rozpoczynajacego gre" << endl;
    cout << "1. Gracz X" << endl;
    cout << "2. Gracz O" << endl;
    cout << "3. Losowo" << endl;

    int option;
    cin >> option;

    while (cin.fail() || option < 1 || option > 3) {
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
        cout << "Wybrano niepoprawna opcje, sprobuj jeszcze raz" << endl;
        cin >> option;
    }

    switch (option) {
        case 1:
            return 'X';
        case 2:
            return 'O';
        case 3:
            return generateRandomPlayer();
    }

    return 0;
}

char generateRandomPlayer() {
    srand(time(nullptr));
    if ((rand() % 2) == 0) {
        return 'X';
    } else {
        return 'O';
    }
}

void display(char board[3][3]) {
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            cout << board[i][j];
        }
        cout << endl;
    }
}

void handleWrongPositionFormat() {
    cin.clear();
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
    cout << "Wprowadzono niepoprawny format pozycji" << endl;
}

void exitProgram(bool &exitFlag, int i) {
    if (i == 9) {
        exitFlag = true;
    }
}

void repeatGame(bool &repeatGameFlag, int i, int oWinStreak, int xWinStreak) {
    if (i == 8) {
        if (oWinStreak != 0 || xWinStreak != 0) {
            int option;
            cout << "Uwaga! Reset gry przerwie serie zwyciestw. Czy na pewno chcesz rozpoczac gre ponownie?" << endl;
            cout << "1. TAK" << endl;
            cout << "2. NIE" << endl;
            cin >> option;

            while (cin.fail() || option < 1 || option > 2) {
                cin.clear();
                cin.ignore(numeric_limits<streamsize>::max(), '\n');
                cout << "Wybrano niepoprawna opcje, sprobuj jeszcze raz" << endl;
                cin >> option;
            }

            if (option == 1) {
                repeatGameFlag = true;
            } else {
                repeatGameFlag = false;
            }

        } else {
            repeatGameFlag = true;
        }
    }
}

void markAnswerOnBoard(char (&board)[3][3], int positionY, int positionX, char character) {
    int x = positionY - 1;
    int y = positionX - 1;

    if (x < 0 || x > 2 || y < 0 || y > 2) {
        throw WrongPositionException(
                "Position (Y - " + to_string(positionY) + ", X - " + to_string(positionX) + ") is not allowed");
    }

    if (board[x][y] != '-') {
        throw CollisionException("Cannot mark character on already filled position");
    }

    board[x][y] = character;
}

bool checkIfWin(char board[3][3], char character) {
    int winStrike = 0;

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            if (board[i][j] == character) {
                winStrike++;
            }
        }

        if (winStrike == 3) {
            return true;
        } else {
            winStrike = 0;
        }
    }

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            if (board[j][i] == character) {
                winStrike++;
            }
        }

        if (winStrike == 3) {
            return true;
        } else {
            winStrike = 0;
        }
    }

    for (int i = 0; i < 3; i++) {
        if (board[i][i] == character) {
            winStrike++;
        }
    }

    if (winStrike == 3) {
        return true;
    } else {
        winStrike = 0;
    }

    int k = 2;
    for (int i = 0; i < 3; i++) {
        if (board[i][k] == character) {
            winStrike++;
        }
        k--;
    }

    if (winStrike == 3) {
        return true;
    } else {
        return false;
    }
}

bool checkIfDraw(char board[3][3]) {
    int filledPositions = 0;

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            if (board[i][j] != '-') {
                filledPositions++;
            }
        }
    }

    if (filledPositions == 9) {
        return true;
    } else {
        return false;
    }
}

void addWins(bool xWin, bool oWin, bool draw, bool repeatGameFlag, int &xWinsNumber, int &xWinStreak, int &oWinsNumber,
             int &oWinStreak) {
    if (xWin) {
        xWinsNumber++;
        xWinStreak++;
        oWinStreak = 0;
    }

    if (oWin) {
        oWinsNumber++;
        oWinStreak++;
        xWinStreak = 0;
    }

    if (draw || repeatGameFlag) {
        xWinStreak = 0;
        oWinStreak = 0;
    }
}
